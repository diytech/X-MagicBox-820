--[[
Author: your name
Date: 2021-08-03 21:56:40
LastEditTime: 2021-08-08 15:43:45
LastEditors: your name
Description: In User Settings Edit
FilePath: \820\sym_gps.lua
--]]

require "math"
local gps = require"gpsZkw"  --820 中科微

local x_pi = 3.14159265358979324 * 3000.0 / 180.0
local pi = 3.1415926535897932384626 
local a = 6378245.0
local ee =  0.00669342162296594323

module(...,package.seeall)


--是否位于国内，国内的经纬度需要转换，国外无需
local function is_out_of_china(lng,lat)
	if (72.004 <= lng)  then
		if(lng <= 137.8347) then 
			if(lat < 0.8293) then
				if(lat > 55.8271) then
					return false
				end
				return true
			end
			return true
		end
		return true
	else
		return true
	end
end


local function transformlat(lng,lat)
    local ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat +0.1 * lng * lat + 0.2 * math.sqrt(math.abs( lng ))
    ret = ret+(20.0 * math.sin(6.0 * lng * pi) + 20.0 *math.sin(2.0 * lng * pi)) * 2.0 / 3.0
    ret = ret+(20.0 * math.sin(lat * pi) + 40.0 *math.sin(lat / 3.0 * pi)) * 2.0 / 3.0
    ret = ret+(160.0 * math.sin(lat / 12.0 * pi) + 320 *math.sin(lat * pi / 30.0)) * 2.0 / 3.0

    return ret
end

local function transformlng(lng,lat)
    local ret =300.0 + lng + 2.0 * lat + 0.1 * lng * lng +0.1 * lng * lat + 0.1 * math.sqrt(math.abs(lng))
    ret = ret + (20.0 * math.sin(6.0 * lng * pi) + 20.0 *math.sin(2.0 * lng * pi)) * 2.0 / 3.0
    ret = ret + (20.0 * math.sin(lng * pi) + 40.0 *math.sin(lng / 3.0 * pi)) * 2.0 / 3.0
    ret = ret +(150.0 * math.sin(lng / 12.0 * pi) + 300.0 *math.sin(lng / 30.0 * pi)) * 2.0 / 3.0

    return ret
end



--wgs84_To_gcj02 坐标转换  
local function wgs84_To_gcj02(lng,lat)
    if  is_out_of_china(lng,lat) then
        local dlat = transformlat(lng-105.0,lat-35.0)
        local dlng = transformlng(lng-105.0,lat-35.0)
        local radlat = lat/180.0 *pi
        local magic = math.sin(radlat)
        magic = 1-ee*magic*magic
        local sqrtmagic =  math.sqrt( magic )
        dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * pi)
        dlng = (dlng * 180.0) / (a / sqrtmagic * math.cos(radlat) * pi)
        local mlat = lat+dlat
        local mlng =  lng+dlng

        return mlng,mlat
    end
end




local function printGps()
    log.info("GPS是否打开:",gps.isOpen())
    if gps.isOpen() then
        local tLocation = gps.getLocation() 
        local speed = gps.getSpeed()
        log.info("是否定位成功:",gps.isFix())
        log.info("可见卫星:",gps.getViewedSateCnt())
        log.info("定位所用卫星个数：", gps.getUsedSateCnt())
        log.info("海拔",gps.getAltitude())
        log.info("速度",speed)
        --需注意，这里读出的经纬度需要纠偏
        --因为读出来的是国际标准 WGS-84 坐标系，国内高德地图使用 GCJ-02 坐标系，百度地图使用 BD-09 坐标系
        log.info("转换前经纬度:",tLocation.lngType,tLocation.lng,tLocation.latType,tLocation.lat)
        
        local lng = tonumber(tLocation.lng)
        local lat = tonumber(tLocation.lat)
        if lng ==nil then
            return -1
        else
            --log.info("转换后经纬度Gcj02",wgs84_To_gcj02(lng,lat))
            local end_lng,end_lat = wgs84_To_gcj02(lng,lat)
            log.info("转换后经纬度Gcj02",end_lng,end_lat)
        end

    end
end



local function test1Cb(tag)
    log.info("test1cb",tag)
end

-- local function nmeaCb(nmeaItem)
--     log.info("testGps.nmeaCb",nmeaItem)
-- end

--如果不调用此接口，默认为GPS+BD定位
--gps.setAerialMode(1,1,0)
--设置NEMA语句的输出频率
--gps.setNemaReportFreq(1,1,1,1)

--设置仅gps.lua内部处理NEMA数据
--如果不调用此接口，默认也为仅gps.lua内部处理NEMA数据
--如果gps.lua内部不处理，把NMEA数据通过回调函数cb提供给外部程序处理，参数设置为1,nmeaCb
--如果gps.lua和外部程序都处理，参数设置为2,nmeaCb
gps.setNmeaMode(0)

gps.open(gps.DEFAULT,{tag="TEST1",cb=test1Cb})
sys.timerLoopStart(printGps,5000)  --每5s查询一次gps位置
