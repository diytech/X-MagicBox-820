--- 模块功能：MYVAT
-- @author JWL
-- @license MIT
-- @copyright JWL
-- @release 2020.04.02

require"ril"

module(..., package.seeall)

--开机就关掉RNDIS 否则物联网卡流量会被网卡给消耗掉。
-- ril.request("AT+RNDISCALL=0,1")

--VAT是否打开 "1" 开 ,"0" 关
local flag_enatc = "1" 
-- 串口ID,串口读缓冲区
local  recvQueue =  {}
-- 串口超时，串口准备好后发布的消息
local uartimeout, recvReady,RECV_MAXCNT = 100, "UART_RECV_ID",1024

--向PC 写入数据，目的是将AT指令的返回数据呈现给电脑一方
local function usb_write(data)
    uart.write(uart.USB, data) 
end

--配置USB 的虚拟串口
uart.setup(uart.USB, 0, 0, uart.PAR_NONE, uart.STOP_1)
uart.on(uart.USB, "receive", function()
    table.insert(recvQueue, uart.read(uart.USB, RECV_MAXCNT))
    sys.timerStart(sys.publish, uartimeout, recvReady)
end)

--处理PC 发过来的AT 指令
function app_procmd(str_recv)
    log.info("str_recv------------",str_recv)

    local flag_handled=true    --用来判断AT指令是否已经被应用层处理，如果已经被处理的就不需要发到底层去了。
    local str_rsp =""          --临时存放应用层已经处理的AT指令返回字符串

    local prefix = string.match(str_recv, "[aA][tT](%+%u+)")
    if prefix ~=nil then
        
        if prefix == "+RIL?" then     --读取VAT标志开关标志
            str_rsp = "+RIL:"..flag_enatc
        elseif prefix == "+RIL" then  --设置VAT 开或者关     
            local temp_enatc = string.match( str_recv, "+RIL=(%d+)")
            if temp_enatc ~= nil then
                flag_enatc = temp_enatc
            end
            if flag_enatc == "0" then  ril.setrilcb(nil) end
            str_rsp = "+RIL:"..flag_enatc
        else
            flag_handled=false
        end
    else
        if  string.upper(str_recv) =="AT\r\n" then
            str_rsp ="OK\r\n"
        else
            flag_handled=false
        end
    end

    if str_rsp ~="" then
        usb_write(str_rsp)
    end

    if (not flag_handled) and (flag_enatc == "1") then
        log.info("send at cmd ==>" ,str_recv)
         ril.setrilcb(usb_write)
         ril.request(str_recv)
    end
end

--将从VAT 串口收到的内容进行拼接
sys.subscribe(recvReady, function()
    local str_recv = table.concat(recvQueue)
    recvQueue = {}
    app_procmd(str_recv)
end)
