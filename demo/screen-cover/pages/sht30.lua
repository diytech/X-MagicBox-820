module(...,package.seeall)
require"tools"

local timer_id

local enable = true
local i2cslaveaddr = 0x44

local function crc_8(data) -- SHT30获取温湿度结果crc校验
    local crc = 0xFF
    local len = #data
    for i = 1, len do
        crc = bit.bxor(crc, data[i])
        for j = 1, 8 do
            crc = crc * 2
            if crc > 0x100 then
                crc = bit.band(bit.bxor(crc, 0x31), 0xff)
            end
        end
    end
    return crc
end

local loading
function update()
    if not enable then
        disp.clear()
        lcd.putStringCenter("板载温湿度计无效",120,120,255,255,255)
        return
    end
    if not loading then
        sys.taskInit(function ()
            loading = true
            i2c.send(tools.i2cid, i2cslaveaddr, {0x2c, 0x06})
            sys.wait(10)
            loading = false
            if not timer_id then return end--屏幕切走了，不继续了
            local r = i2c.recv(tools.i2cid, i2cslaveaddr, 6)
            if #r ~= 6 then return end

            --from 如果能编程回忆
            local t, h -- 定义局部变量，用以保存温度值和湿度值
            local tempCrc = {} -- 定义局部表，保存获取的温度数据，便于进行crc校验
            local humiCrc = {} -- 定义局部表，保存获取的湿度数据，便于进行crc校验
            local _, b, c, d, e, f, g = pack.unpack(r, "b6")
            table.insert(tempCrc, b) -- 将温度高八位和温度低八位存入表中，稍后进行crc校验
            table.insert(tempCrc, c)
            table.insert(humiCrc, e) -- 将湿度高八位和湿度低八位存入表中，稍后进行crc校验
            table.insert(humiCrc, f)

            local result1 = crc_8(tempCrc) -- 温度数据crc校验
            local result2 = crc_8(humiCrc) -- 湿度数据crc校验

            if d == result1 and g == result2 then -- 将数据放大100倍，便于不带float的固件使用
                t = ((4375 * (b * 256 + c)) / 16384) - 4500 --根据SHT30传感器手册给的公式计算温度和湿度
                h = ((2500 * (e * 256 + f)) / 16384)
            else
                log.warn("sht30","crc校验失败")
                return
            end

            disp.clear()
            disp.setfontheight(32)
            lcd.CHAR_WIDTH = 16
            lcd.putStringCenter(string.format("温度：%0.2fC",t/100),120,90,255,100,100)
            lcd.putStringCenter(string.format("湿度：%0.2f%%",h/100),120,150,100,100,255)
            disp.setfontheight(16)
            lcd.CHAR_WIDTH = 8
        end)
    end
end

function open()
    disp.clear()
    lcd.putStringCenter("数据加载中...",120,120,255,255,255)
    disp.update()
    timer_id = sys.timerLoopStart(page.update,500)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end

