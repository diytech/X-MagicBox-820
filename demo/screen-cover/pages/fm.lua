module(...,package.seeall)

--当前的开关状态
local enable = false

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("在线电台",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("当前状态",120,lcd.gety(3),255,150,255)
    lcd.putStringCenter(enable and "开" or "关",120,lcd.gety(5),255,0,0)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.putStringCenter("中央人民广播电台",120,lcd.gety(3),100,100,100)
end

local keyEvents = {
    ["3"] = function ()
        enable = not enable
        if enable then sys.publish("FM_ENABLE") end
        --开始或者结束播放
    end,
}
keyEvents["A"] = keyEvents["3"]
keyEvents["OK"] = keyEvents["3"]

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function open()
    audio.stop()
end

--缓存的声音流数据
local ftemp = {}

function close()
    enable = false--停止播放
    audio.stop()
    ftemp = {}
end

sys.taskInit(function()
    local r, s, p
    while true do
        while not socket.isReady() or not enable do
            sys.waitUntil("FM_ENABLE",1000)
        end
        local c = socket.tcp()
        while not c:connect("home.papapoi.com", "23456") do sys.wait(2000) end
        sys.publish("SOCKET_CONNECTED")
        while enable do
            r, s, p = c:recv(120000, "SOCKET_SEND_DATA_FM")
            if r then
                table.insert(ftemp,s)
                sys.publish("FM_DATA_RECV")
            elseif s == "SOCKET_SEND_DATA_FM" then
                log.info("socket send", p:sub(1,50))
                if not c:send(p) then break end
            elseif s == "timeout" then
                --没必要处理这东西
            else
                break
            end
        end
        r,s,p = nil,nil,nil
        c:close()
        --link.shut()
    end
end)

sys.taskInit(function()
    while true do
        while #ftemp == 0 do sys.waitUntil("FM_DATA_RECV",1000) end
        local data = table.remove(ftemp,1)
        --audiocore.stop()
        local c = 1
        while enable do
            c = c + audiocore.streamplay(audiocore.MP3,data:sub(c,-1))
            if c>=#data then
                break
            elseif c == 0 then
                log.error("fm", "AudioStreamPlay Error")
                break
            end
            sys.wait(5)
        end
        sys.wait(5)
        if not enable then ftemp = {} audiocore.stop() end
    end
end)
